#![feature(proc_macro, wasm_custom_section, wasm_import_module)]

extern crate wasm_bindgen;

use wasm_bindgen::prelude::*;

// import a function from JavaScript
#[wasm_bindgen]
extern {
    fn alert(s: &str);
}

// export a function to JavaScript and use the imported function from JavaScript
#[wasm_bindgen]
pub fn greet(name: &str) {
    alert(&format!("Hello, {}!", name));
}
